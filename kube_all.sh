#!/bin/bash

# A script to run kubectl commands across all contexts

usage() {
    echo "Usage: ./kubectl_all.sh <kubectl_command> [additional_arguments] [namespace] [--dry-run] [--interactive] [--verbose] [--quiet] [--stop-on-error] [--exclude-contexts context1,context2,...]"
    exit 1
}

# Check if kubectl is installed
if ! command -v kubectl &> /dev/null
then
    echo "kubectl is not installed. Please install it first."
    exit 1
fi

# Check if at least one argument is provided
if [ $# -eq 0 ]
then
    usage # Call the usage function
fi


# Parse arguments
KUBECTL_COMMAND=$1 # The kubectl command to run
shift # Shift the arguments to the left
ADDITIONAL_ARGUMENTS=() # An array of additional arguments for the kubectl command
NAMESPACE="" # The namespace to use, if any
DRY_RUN=false # A flag to indicate whether to do a dry run or not
INTERACTIVE=false # A flag to indicate whether to prompt for confirmation or not
VERBOSE=false # A flag to indicate whether to print extra output or not
QUIET=false # A flag to indicate whether to suppress console output or not
STOP_ON_ERROR=false # A flag to indicate whether to stop the script if a command fails or not
EXCLUDE_CONTEXTS=() # An array of contexts to exclude from running the command

while [ $# -gt 0 ]
do
    case "$1" in
        --dry-run)
            DRY_RUN=true
            shift
            ;;
        --interactive)
            INTERACTIVE=true
            shift
            ;;
        --verbose)
            VERBOSE=true
            shift
            ;;
        --quiet)
            QUIET=true
            shift
            ;;
        --stop-on-error)
            STOP_ON_ERROR=true
            shift
            ;;
        --exclude-contexts)
            IFS=',' read -ra EXCLUDE_CONTEXTS <<< "$2"
            shift 2
            ;;
        *)
            # Check if the argument is a valid namespace name
            if kubectl get namespace "$1" &> /dev/null
            then
                NAMESPACE="$1"
                shift
            else
                # Add the argument to the additional arguments array
                ADDITIONAL_ARGUMENTS+=("$1")
                shift
            fi
            ;;
    esac
done

# Check if the kubectl command is valid
if ! kubectl help "$KUBECTL_COMMAND" &> /dev/null 
then 
    echo "Invalid kubectl command: $KUBECTL_COMMAND"
    exit 1 
fi

# Check if the user wants to delete something and ask for confirmation if so 
if [ "$KUBECTL_COMMAND" == "delete" ] && [ "$DRY_RUN" == false ] && [ "$INTERACTIVE" == false ]
then 
    read -p "Are you sure you want to delete across all contexts? This cannot be undone. (y/n) " answer 
    case ${answer:0:1} in 
        y|Y ) 
            ;; 
        * ) 
            echo "Aborting." 
            exit 0 
            ;; 
    esac 
fi 

# Get the current context name 
CURRENT_CONTEXT=$(kubectl config current-context)

# Get all the context names from the kubectl config file 
ALL_CONTEXTS=($(kubectl config get-contexts -o name))

# Loop through each context and run the command 
for CONTEXT in "${ALL_CONTEXTS[@]}"
do 
    # Check if the context is in the exclude list 
    if [[ " ${EXCLUDE_CONTEXTS[@]} " =~ " ${CONTEXT} " ]]
    then 
        continue # Skip this context 
    fi 

    # Switch to the context 
    kubectl config use-context "$CONTEXT" &> /dev/null 

    # Build the full command with the arguments and namespace 
    FULL_COMMAND="kubectl $KUBECTL_COMMAND ${ADDITIONAL_ARGUMENTS[*]}"
    if [ -n "$NAMESPACE" ]
    then 
        FULL_COMMAND="$FULL_COMMAND -n $NAMESPACE"
    fi 

    # Print the context and the command if not quiet mode 
    if [ "$QUIET" == false ]
    then 
        echo "Context: $CONTEXT"
        echo "Command: $FULL_COMMAND"
    fi 

    # Check if dry run mode is on 
    if [ "$DRY_RUN" == true ]
    then 
        # Check if the kubectl command supports the --dry-run flag
        if [[ "$KUBECTL_COMMAND" =~ ^(apply|create|delete|patch|replace|run)$ ]]
        then
            # Append the --dry-run=client flag to the command
            FULL_COMMAND="$FULL_COMMAND --dry-run=client"
            OUTPUT=$(eval "$FULL_COMMAND")
            EXIT_CODE=$?    
        else
            # Print a warning message and skip this command
            echo "Warning: $KUBECTL_COMMAND does not support the --dry-run flag. Skipping this command."
            continue # Skip this command and go to the next context
        fi
    else 
        # Check if interactive mode is on 
        if [ "$INTERACTIVE" == true ]
        then 
            # Ask for confirmation before running the command 
            read -p "Do you want to run this command? (y/n) " answer 
            case ${answer:0:1} in 
                y|Y ) 
                    ;; 
                * ) 
                    echo "Skipping this command." 
                    continue # Skip this command and go to the next context 
                    ;; 
            esac 
        fi 

        # Run the command and capture the output and exit code 
        OUTPUT=$(eval "$FULL_COMMAND")
        EXIT_CODE=$?
    fi 

    # Print the output if verbose mode is on or if the command failed 
    if [ "$VERBOSE" == true ] || [ $EXIT_CODE -ne 0 ] || [ "$DRY_RUN" == false ]
    then 
        echo "$OUTPUT"
    fi 

    # Log the output to a file with the context name 
    LOG_FILE="kubectl_all_${CONTEXT}.log"
    echo "$OUTPUT" > "$LOG_FILE"

    # Check if the command failed and stop on error mode is on 
    if [ $EXIT_CODE -ne 0 ] && [ "$STOP_ON_ERROR" == true ]
    then 
        echo "Command failed with exit code $EXIT_CODE. Stopping the script."
        break # Break out of the loop 
    fi 

done

# Switch back to the original context 
kubectl config use-context "$CURRENT_CONTEXT" &> /dev/null

# Print a final message if not quiet mode 
if [ "$QUIET" == false ]
then 
    echo "Done."
fi

# Kubectl Multi-Context Script

This script is designed to automate the process of running kubectl commands across multiple contexts. It's particularly useful when managing multiple Kubernetes clusters.

## Installation

Clone the repository:

```
git clone https://gitlab.com/Cjoga/kube_all
```

Navigate to the repository:

```
cd kubectl_all
```

Make the script executable:

```
chmod +x kubectl_all.sh
```

## Usage

The script is called with the desired `kubectl` command and additional arguments. The command is then executed for all contexts, unless a specific list of contexts to exclude is provided. You can specify a namespace if necessary, and use the `--dry-run` flag to see what the script will do without making actual changes.

```
./kubectl_all.sh <kubectl_command> [additional_arguments] [namespace] [--dry-run] [--interactive] [--verbose] [--quiet] [--stop-on-error] [--exclude-contexts context1,context2,...]
```

**Note:** This script relies on your current kubectl configuration and the contexts defined therein.

### Arguments

* `<kubectl_command>`: The kubectl command you want to execute. This is a required argument.
* `[additional_arguments]`: Any additional arguments for the kubectl command.
* `[namespace]`: The Kubernetes namespace in which to run the command. If not provided, the default namespace is used.
* `--dry-run`: With this flag, the script will print what would be done without actually executing the commands.
* `--interactive`: This flag will prompt you to confirm each command before it's executed.
* `--verbose`: Use this flag to print extra output for troubleshooting purposes.
* `--quiet`: Use this flag to suppress console output. Log will still be written to a file.
* `--stop-on-error`: This flag stops the script if a `kubectl` command fails.
* `--exclude-contexts`: A comma-separated list of contexts to exclude. The command will not be run in these contexts.

### Logs

Each execution of a command is logged in a separate file with the prefix "kubectl_all", followed by the context's name.


## Warning

Use this script with caution, especially with destructive commands like `delete`. The script will ask for confirmation before executing a delete command across all contexts, but the responsibility for ensuring it doesn't cause unwanted destruction is ultimately yours.

## Author
    
Created by [Camilo Joga](https://gitlab.com/Cjoga).


## Contributing

Contributions are welcome! Please feel free to submit a Pull Request.